﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;

namespace TP08_exception
{
    public class Program
    {
        public static double RacineCarree(double valeur)
        {
            if (valeur <= 0)
                throw new ArgumentOutOfRangeException("valeur", "Le paramètre doit être positif");
            return Math.Sqrt(valeur);
        }
        static void Main(string[] args)
        {
            List<int> values = new List<int> { 17, 12, 15, 38, 29, 157, 89, -22, 0, 5 };
            MultipleDivision mltd = new MultipleDivision(values);
            Console.WriteLine("Quel dividende ? ");
            int dividende = int.Parse(Console.ReadLine());
            Console.WriteLine("Quel diviseur ? ");
            int diviseur = int.Parse(Console.ReadLine());
            
            Console.WriteLine(mltd.Division(dividende, diviseur));

            Console.ReadKey();
        }
    }
}
