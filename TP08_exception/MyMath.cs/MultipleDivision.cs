﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMath
{
    public class MultipleDivision
    {
        private List<int> values;
        public MultipleDivision(List<int> values)
        {
            this.values = values;
        }

        public double Division(int index_dividende, int index_diviseur)
        {
            double quotient;
            quotient = this.values[index_dividende] / (double)this.values[index_diviseur];
            return quotient;
        }
    }
}
